
var Rendition = function() {
    return this;
}

Rendition.prototype = new Model();

Rendition.prototype.path = null;

Rendition.prototype.fields = ['id', 'media_id', 'name', 'url', 'size', 'expires', 'width', 'height', 'created_at', 'updated_at', 'mime', 'video_codec', 'audio_codec', 'filename'];

Rendition.prototype.mutable = [];

Rendition.prototype.has_status = false;
