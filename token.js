
var Token = function() {
    return this;
}

Token.prototype = new Model();

Token.prototype.path = 'auth';

Token.prototype.fields = ['token', 'company_id', 'level_id', 'expires', 'updated_at', 'created_at', 'id', 'permissions', 'company'];

Token.publicAuth = function(callback, slug) {
    var t = new Token().call(function(result) {

        // Save the results
        if (result.success) result.process();

        callback(result);

    }, '/public', {slug: slug});
}

Token.prototype.process = function() {
    if (this.company)       Api.company = this.company;
    if (this.user)          Api.company = this.user;
    if (this.permissions)   Api.company = this.permissions;

    if (this.company && this.company.boltons)
        Api.boltons = this.company.boltons;

    if (this.user && this.user.permissions)
        Api.permissions = this.user.boltons;
}
