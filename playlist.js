
var Playlist = function() {
    return this;
}

Playlist.prototype = new Model();

Playlist.prototype.path = 'playlists';

Playlist.prototype.fields = ['id', 'company_id', 'user_id', 'name', 'created_at', 'updated_at', 'status', 'slug'];

Playlist.prototype.mutable = ['name', 'status'];

Playlist.prototype.has_status = true;

Playlist.prototype.relations = [
    {
        key: 'items',
        model: 'Item',
        multiple: true,
        request: function(callback, item) {
            return new Item().where('playlist_id', item.field('id')).get(callback)
        }
    },
    {
        key: 'company',
        model: 'Company',
        multiple: false,
        request: function(callback, item) {
            return new Company().find(callback, item.field('id'));
        }
    }
];

Playlist.prototype.addItem = function(callback, item_id, field) {
    // Default to adding item_id, but accept, e.g., media_id
    var data = {};
    if (field) {
        data[field] = item_id;
    } else {
        data.item_id = item_id;
    }

    this.call(callback, '/add', data, 'POST');
}


Playlist.prototype.removeItem = function(callback, item_id, field) {
    // Default to adding item_id, but accept, e.g., media_id
    var data = {};
    if (field) {
        data[field] = item_id;
    } else {
        data.item_id = item_id;
    }

    this.call(callback, '/remove', data, 'POST');
}
