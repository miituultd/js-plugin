var Media = function() {
    return this;
}

Media.prototype = new Model();

Media.prototype.path = 'media';

Media.prototype.fields = ['id', 'company_id', 'type', 'duration', 'start_time', 'end_time', 'status', 'created_at', 'updated_at', 'slug', 'name', 'panda_id'];

Media.prototype.mutable = ['start_time', 'end_time', 'status', 'name'];

Media.prototype.has_status = true;


Media.video_formats = ['h264.hi', 'webm.hi', 'ogg.hi', 'thumbnail', 'animail_medium', 'animail_small'];
Media.auto_formats = ['m4a', 'mp3', 'oga'];

Media.prototype.relations = [
    {
        key: 'renditions',
        model: 'Rendition',
        multiple: true,
        request: function(callback, media) {
            throw new Error('Renditions should always be returned with media.');
        }
    },
    {
        key: 'answer',
        model: 'Answer',
        multiple: false,
        request: function(callback, media) {
            throw new Error('Cannot currently fetch answer from media.');
        }
    },
    {
        key: 'item',
        model: 'Item',
        multiple: false,
        request: function(callback, media) {
            throw new Error('Cannot currently fetch item from media.');
        }
    }
];

Media.prototype.url = function(name) {

    var url = null;

    this.relation(function(renditions) {
        renditions.each(function(rendition, i) {

            if (rendition.field('name') == name) {
                url = rendition.field('url');
            }

        });
    }, 'renditions', false);

    return url ? url : null;
}

/*
 *  Return the URL from a rendition specified by it's name
 *  Optionally $names is an array,, the first available option will be returned
 *
public function url($names, $dynamic = false, $download = false) {
    // Make sure we have an array to loop through
    if (!is_array($names)) $names = array($names);

    foreach ($names as $name) {

        // If an invalid media name has been submitted, don't even bother
        if (!in_array($name, self::$video_formats) && !in_array($name, self::$audio_formats)) continue;

        // If a dynamic URL has been requested, we build it from the id etc
        if ($dynamic) {
            return Api::$base . $this->path . 'rendition/' . $this->id . '/' . $name . ($download ? '/1' : '');

        // But non-dynamic URLs go straight to the return S3 URL
        } else {
            if (!$this->renditions) return false;
            foreach ($this->renditions as $rendition) {
                if ($rendition->name == $name) return $rendition->url;
            }
        }
    }

    // We couldn't find anything good to return...
    return false;
}

/*
 *  Return the rendition object specified by name
 *
public function rendition($name) {
    // If an invalid media name has been submitted, reject it outright
    if (!in_array($name, self::$video_formats) && !in_array($name, self::$audio_formats)) return false;

    // Find the correct rendition
    foreach ($this->renditions as $rendition) {
        if ($rendition->name == $name) return $rendition;
    }

    // We couldn't find anything good to return...
    return false;
}
*/
