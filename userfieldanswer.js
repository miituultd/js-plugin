
var Userfieldanswer = function() {
    return this;
}

Userfieldanswer.prototype = new Model();

Userfieldanswer.prototype.path = null;

Userfieldanswer.prototype.has_status = false;

Userfieldanswer.prototype.fields = ['id', 'userfield_id', 'userfieldanswer_id', 'created_at', 'updated_at', 'answer', 'status'];

Userfieldanswer.prototype.mutable = [];

Userfieldanswer.prototype.relations = [
    {
        key: 'respondent',
        model: 'Respondent',
        multiple: false,
        request: function(callback, item) {
            new Respondent().where('id', item.field('respondent_id')).get(callback);
        }
    },
    {
        key: 'userfield',
        model: 'Userfield',
        multiple: false,
        request: function(callback, item) {
            new Userfield().where('id', item.field('respondent_id')).get(callback);
        }
    }
];
