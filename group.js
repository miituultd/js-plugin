
var Group = function() {
    return this;
}

Group.prototype = new Model();

Group.prototype.path = 'groups';

Group.prototype.fields = ['id', 'company_id', 'user_id', 'name', 'created_at', 'updated_at', 'status'];

Group.prototype.mutable = ['name', 'status'];

Group.prototype.has_status = true;

Group.prototype.relations = [
    {
        key: 'questionnaires',
        model: 'Questionnaire',
        multiple: true
    }
];
