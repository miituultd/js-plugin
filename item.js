
var Item = function() {
    return this;
}

Item.prototype = new Model();

Item.prototype.path = 'items';

Item.prototype.fields = ['id', 'company_id', 'media_id', 'image_id', 'status', 'title', 'description', 'created_at', 'updated_at'];

Item.prototype.mutable = ['status', 'title', 'description', 'created_at', 'updated_at'];

Item.prototype.has_status = true;

Item.types = ['image', 'audio', 'video'];

Item.prototype.relations = [
    {
        key: 'playlists',
        model: 'Playlist',
        multiple: true,
        request: function(callback, item) {
            throw new Error('Cannot currently fetch playlist from item.');
        }
    },
    {
        key: 'media',
        model: 'Media',
        multiple: false,
        request: function(callback, item) {
            throw new Error('Cannot currently fetch media from item.');
        }
    },
    {
        key: 'company',
        model: 'Company',
        multiple: false,
        request: function(callback, item) {
            return new Company().find(callback, item.field('id'));
        }
    }
];
