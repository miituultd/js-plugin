var Api = function() {

}

// Yes, these would be more convenient the other way round
// Always compare them using the Api.level() function
Api.LEVEL_PUBLIC            = 10;
Api.LEVEL_TRUSTED           = 20;
Api.LEVEL_ADMIN             = 30;
Api.LEVEL_OWNER             = 40;
Api.LEVEL_MIITUU            = 50;

// New status constants
Api.STATUS_PRIVATE          = 10;
Api.STATUS_PROTECTED        = 20;
Api.STATUS_PUBLIC           = 30;

Api.STATUS_FAILED           = 60;
Api.STATUS_PROMISED         = 70;
Api.STATUS_PENDING          = 80;
Api.STATUS_DELETED          = 90;

// Active/inactive are used for administrators etc
Api.STATUS_INACTIVE         = 10;
Api.STATUS_ACTIVE           = 30;

// Used for uploads
Api.STATUS_UPLOADED         = 80;
Api.STATUS_COMPLETE         = 20;

// Used for archives
Api.STATUS_QUEUED           = 70;

// These will be filled with model details when authentication happens
Api.token                   = false;
Api.company                 = false;
Api.user                    = false;
Api.permissions             = false;
Api.boltons                 = false;

// Build an array of each call that's made, for debugging purposes
Api.calls                   = [];

// These URLs are used for different environments, e.g. development and test
// These are only used for internal miituu testing, you should not change these
Api.base_urls               = {
    'live'  : 'https://api.miituu.com/',
    'stage' : 'http://stage-api.miituu.com/',
    'dev'   : 'http://api.miituu.dev/'
}

Api.base                    = 'https://api.miituu.com/';
Api.environment             = 'live';



/*
 *  This allows us to change the API environment, e.g. developer and test
 *  This is only used for internal miituu testing, you should not use this
 */
Api.setEnvironment = function(environment)
{
    // If a base URL doesn't exist for it, it's not an acceptable environment
    if (!Api.base_urls[environment]) {
        throw new Error('Unknown miituu API environment '+environment);
    }

    // Make the change
    Api.environment = environment;
    Api.base = Api.base_urls[environment];
}


/*
 *  Take a company slug and start a session
 */
Api.publicAuth = function(callback, slug)
{
    // Public auth is controlled via the token model
    Token.publicAuth(function(token) {
        Api.token = token;
        callback(token);
    }, slug);
}

/*
 *  Return a string with the auth data for restoring a session later
 *  Or, if a string is passed it, it will be used but NOT checked
 */
Api.authStr = function( authStr )
{
    if (authStr) Api.token = new Token().fill({'token': authStr});

    if (!Api.hasAuth()) return false;

    return Api.token.token;
}

/*
 *  Restore auth details from a token
 *  Optionally accepts a public slug which it will re-auth with if necessary
 *  Optionally also an array of objects related to the user to request too
 */
Api.restore = function(token, slug, include)
{
    Api.token = Token.fill({'token': token});

    // We can't method chain this check, as Api.prototype.token won't exist by the time the call is made
    Api.token.check(include);

    // If restore failed and we have a slug, try publicAuth
    if (!Api.token.success && slug) {
        return Api.publicAuth(slug);
    }

    return Api.token;
}

/*
 *  Calls the API to check the auth is valid and refresh all user/company/auth data
 *  This is not usually necessary, as the auth and restore methods all do this
 */
Api.checkAuth = function(include) {
    return Token.check(include);
}

/*
 *  Returns true if there are current auth details, but does not validate them with the API
 */
Api.hasAuth = function() {
    return Api.token ? true : false;
}

/*
 *  Login an existing user using email address and password
 */
Api.login = function(email, password)
{
    Api.token = Token.login(email, password);

    return Api.token;
}

/*
 *  Returns the current company, usually without calling the API
 */
Api.currentCompany = function() {
    if (Api.company) {
        return Api.company;

    } else {
        return Company.get();
    }
}

/*
 *  Returns the current user, usually without calling the API
 */
Api.currentUser = function() {
    if (Api.user) {
        return Api.user;

    } else {
        return User.get();
    }
}

/*
 *  If a level is provided, return true if the current auth level is at or higher
 *  If exact_match is true, only return true if the level matches (not higher)
 *  See top for file level constants
 *  PLEASE NOTE: Lower numbers indicate higher auth level
 *  If a level is not provided, return the current auth level, or false
 */
Api.level = function( level, exact_match )
{
    if (level && exact_match) {
        return (Api.prototype.token && Api.prototype.token.level_id && Api.prototype.token.level_id == level);

    } else if (level) {
        return (Api.prototype.token && Api.prototype.token.level_id && Api.prototype.token.level_id <= level);

    } else {
        return (Api.prototype.token && Api.prototype.token.level_id) ? Api.prototype.token.level_id : false;
    }
}

/*
 *  Return true if the current auth allows the provided permission
 *  See the docs for a list of all possible permissions
 */
Api.can = function( permission )
{
    // If no permissions are loaded, the answer is no
    if (!Api.permissions || typeof Api.permissions != Array) return false;

    for (var i = 0; i < Api.permissions.length; i++) {
        if (el(perm, 'slug') == Api.permission[i]) return true;
    }

    return false;
}


/*
 *  Return a list of all calls made so far by the API, for debugging purposes
 *  Optionally [slightly] formatted, default
 */
Api.calls = function( format )
{
    if (!format) return Api.calls;

    var html = '<table>';
    for (var i = 0; i < Api.calls.length; i++) {
        var call = Api.calls[i];
        for (var j in calls) {
            html += '<tr><th>'+j+':</th><td>'+call[j]+'</td></tr>';
        }
        html += '<tr><td colspan="2"><hr></td></tr>';
    }
    html += '</table>';

    return html;
}

/*
 *  Helper function to return an element or a default
 */
Api.el = function(data, field, _default) {
    if (data[field] !== undefined) {
        return data[field];
    } else {
        return _default;
    }
}

/*
 *  Helper function to merge two objects
 */
Api.mergeObjects = function(obj1,obj2){
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
}
