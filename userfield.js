
var Userfield = function() {
    return this;
}

// Constants used for the type field, and an array of all valid numbers
Userfield.TYPE_OTHER   = 0;
Userfield.TYPE_EMAIL   = 1;
Userfield.TYPE_NAME    = 2;

Userfield.prototype = new Model();

Userfield.prototype.path = null;

Userfield.prototype.has_status = false;

Userfield.prototype.fields = ['id', 'questionnaire_id', 'title', 'order', 'created_at', 'updated_at', 'required', 'status', 'type'];

Userfield.prototype.mutable = ['title', 'order', 'required', 'status', 'type'];

Userfield.prototype.relations = [
    {
        key: 'questionnaire',
        model: 'Questionnaire',
        multiple: false,
        request: function(callback, item) {
            new Questionnaire().where('id', item.field('questionnaire_id')).get(callback);
        }
    },
    {
        key: 'userfieldanswers',
        model: 'Userfieldanswer',
        multiple: true,
        request: function(callback, item) {
            new Userfieldanswer().where('userfield_id', item.field('id')).get(callback);
        }
    }
];
