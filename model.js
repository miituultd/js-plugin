
var Model = function() {
    // return this;
}

Model.prototype = Api.prototype;

// Data to be sent with each request
Model.prototype.params          = {};

// Which relations we'll need to include
Model.prototype.withs           = [];

// Position we are at while looping multiple items
Model.prototype.position        = 0;
// The multiple items we're looping through
Model.prototype.items           = [];

// There are set per model
Model.prototype.fields          = [];
Model.prototype.mutable         = [];
Model.prototype.messages        = [];

Model.prototype.has_status      = false;

Model.prototype.clean           = {};
Model.prototype.dirty           = {};
Model.prototype.relations       = {};

// Paging information
Model.prototype.current_page    = null;
Model.prototype.total_items     = null;
Model.prototype.total_pages     = null;
Model.prototype.per_page        = null;
Model.prototype.next_page       = null;
Model.prototype.prev_page       = null;

// These can be overridden for specific models
Model.prototype.status_titles   = {};
Model.prototype.status_titles[Api.STATUS_PRIVATE]   = 'Private';
Model.prototype.status_titles[Api.STATUS_PROTECTED] = 'Protected';
Model.prototype.status_titles[Api.STATUS_PUBLIC]    = 'Public';
Model.prototype.status_titles[Api.STATUS_FAILED]    = 'Failed';
Model.prototype.status_titles[Api.STATUS_PROMISED]  = 'Promised';
Model.prototype.status_titles[Api.STATUS_PENDING]   = 'Pending';
Model.prototype.status_titles[Api.STATUS_DELETED]   = 'Deleted';


// TODO: Paging info

Model.prototype.call = function(callback, _endpoint, _data, _method, _auth, return_as) {
    // Prepare the variables well need, or use the defaults
    var endpoint = _endpoint != undefined ? _endpoint : '',
        data     = _data     != undefined ? _data     : {},
        method   = _method   != undefined ? _method   : 'GET',
        auth     = _auth     != undefined ? _auth     : true;

    // Get the global paging info
    if (this.per_page)     data.per_page = this.per_page;
    if (this.current_page) data.page     = this.current_page;

    // Merge pre-existing params and new data
    var data = Api.mergeObjects(this.params, data);

    //IE8 needs to use XDomainRequest object for cross domain calls (api)
    var xhr = (window.XDomainRequest) ? new window.XDomainRequest() : new XMLHttpRequest();

    var url = Api.base + this.path + endpoint;

    // Build a query string with the data
    if (data) {
        var params = [];
        for (var p in data) {
            if (data.hasOwnProperty(p)) {
                params.push(encodeURIComponent(p) + "=" + encodeURIComponent(data[p]));
            }
        }
    }

    // Add in the withs
    if (this.withs.length)
        params.push( 'include='+this.withs.join(',') );

    //IE8 XDomain object doesn't support adding headers to the request, send the token with the request parameters
    if( !xhr.setRequestHeader ) {
        params.push( 'token=' + encodeURIComponent( Api.token.field('token') ) );
    }

    // Use it as a query string?
    if (method == 'GET' || method == 'DELETE') {
        url += '?' + params.join('&');
        dataStr = null;

    // Or send it in POST?
    } else {
        dataStr = params.join('&');
    }

    // Now we can start preparing the actual XHR...
    xhr.open(method, url, true);

    //More capable browsers can add headers to the request, send the token in the header
    if( xhr.setRequestHeader ) {
        if (method == 'POST') xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        // Add the token as a header?
        if (auth && Api.token) xhr.setRequestHeader('App-Token', Api.token.field('token'));
    }

    // We'll need to access this scope inside the xhr callback
    var model = return_as || this;

    // Define a handler for the result
    ( window.XDomainRequest ) ? this.xdrHandler( xhr, model, callback ) : this.xhrHandler( xhr, model, callback );

    // And away we go!
    xhr.send(dataStr);
}

/*
 *  Handler for browsers using XMLHttpRequest object
 */
Model.prototype.xhrHandler = function( xhrObject, model, callback ) {

    xhrObject.onreadystatechange =  function() {

        if ( this.readyState == 4 ) {
            var body = model.parseJSON( this.responseText );

            if ( !body ) {
                // We've not been able to process the server response
                model.onAjaxFail( 'Sorry, there was a problem communicating with the API.' );
                return callback(model);
            }

            // A good server response
            if ( this.status == 200 ) {
                model.onAjaxSuccess( body );
            } else {
                model.onAjaxFail( body.messages );
            }

            // And we're done.
            callback(model);
        }
    }

    return true;
}

/*
 *  Handler for browsers using XDomainRequest object
 */
Model.prototype.xdrHandler = function( xdrObject, model, callback ) {

    xdrObject.onload =  function() {
        var body = model.parseJSON( this.responseText );

        if ( !body ) {
            // We've not been able to process the server response
            model.onAjaxFail( 'Sorry, there was a problem communicating with the API.' );
            return callback(model);
        }

        model.onAjaxSuccess( body );

        // And we're done.
        callback(model);
    }

    //IE9 needs this handler to be defined for xdr to work
    xdrObject.onprogress = function() {};

    xdrObject.onerror =  function() {
        model.onAjaxFail( 'Sorry, there was a problem communicating with the API.' );
        return callback(model);
    }

    return true;
}

/*
 *  Fail handler for xhr and xdr requests, after onerror or xhr.status != 200
 */
Model.prototype.onAjaxFail = function( messages ) {

    /* it's all a scope thing, the xhr or xdrHandler will call these functions (success or fail)
    and set the model's success status, inside the ajax handlers model refers to this (Model.prototype or whatever extends model)
    but here we are outside the ajax handler's scope */
    this.success = false;

    if ( Object.prototype.toString.call( messages ) === '[object Array]' ) {
        this.messages = messages;
    } else {
        this.messages.push( messages );
    }

    return true;
}


/*
 *  Success handler for xhr and xdr requests, after onload or xhr.status = 200
 */
Model.prototype.onAjaxSuccess = function( body ) {
    //if unsure on what is the context for 'this' check comments above
    this.success = true;

    // We've received multiple items
    if ( body.body ) {
        this.items = body.body;
        this.setPaging( body );
        this.first;
    } else {
        this.fill( body );
    }

    return true;
}

/*
 *  Parse a data string and return a JS object or false on failure
 */
Model.prototype.parseJSON = function( data ) {

    try {
        var json = JSON.parse( data );
    } catch(e) {
        return false;
    }

    return json;
}

/*
 *  Delete an item
 */
Model.prototype.remove = function(callback, _id) {
    if (this.exists() && !_id) {
        id = this.field('id');

    } else if (!this.exists() && _id) {
        id = _id;

    } else {
        throw new Error('Invalid conditions for set_status()');
    }

    return this.call( callback, '', {id: id}, 'DELETE');
}

/*
 *  Only applicable to certain models, creates a promised media object
 */
Model.prototype.promiseMedia = function(callback, type) {
    if (type) this.field('type', type);

    var media = new Media();
    return this.call(callback, '/promise_media', {type: this.field('type'), id: this.field('id')}, 'GET', true, media);
}

/*
 *  Specify how many items should be taken per page
 */
Model.prototype.take = function(number) {
    this.per_page = number;

    return this;
}

/*
 *  Specify how many items should be taken per page (just wraps take)
 */
Model.prototype.perPage = function(number) {
    return this.take(number);
}

/*
 *  Specify which page we should take
 */
Model.prototype.page = function(number) {
    this.current_page = number;

    return this;
}

/*
 *  Save the information from a response about how many pages are available etc
 */
Model.prototype.setPaging = function(data) {
    this.current_page  = Api.el(data, 'current_page');
    this.total_items   = Api.el(data, 'total_items');
    this.total_pages   = Api.el(data, 'total_pages');
    this.per_page      = Api.el(data, 'per_page');

    this.next_page     = this.current_page < this.total_pages ? this.current_page + 1 : null;
    this.prev_page     = this.current_page > 1 ? this.current_page - 1 : null;
}

/*
 *  Specify how the results should be ordered
 */
Model.prototype.orderBy = function(field, direction) {
    this.where('sort', field);

    if (direction) this.where('order', direction);

    return this;
}

/*
 *  Fill the model with the provided data
 */
Model.prototype.fill = function(data) {
    this.reset();

    for (var i = 0; i < this.fields.length; i++) {
        this.clean[ this.fields[i] ] = Api.el( data, this.fields[i] );
    }

    for (var i = 0; i < this.relations.length; i++) {
        // If we have an item in the incoming data for this relations key
        if (Api.el(data, this.relations[i].key) ) {
            var model_data = Api.el(data, this.relations[i].key);

            // TODO: new window[ modelName ] might not work everywhere, particularly Ti, need to test this!

            if (this.relations[i].multiple) {
                this.relations[i].data = new window[ this.relations[i].model ]().setItems( model_data );
            } else {
                this.relations[i].data = new window[ this.relations[i].model ]().fill( model_data );
            }
        }
    }

    return this;
}

/*
 *  Fill a model with multiple items, used when creating included related models
 */
Model.prototype.setItems = function(data) {
    this.items = data;

    return this;
}

/*
 *  Assuming there's a set of data available, fill and return the first item
 */
Model.prototype.first = function() {
    return this.item( 0 );
}

/*
 *  Assuming there's a set of data available, fill and return the requested
 */
Model.prototype.item = function(x) {
    if (x < 0 || x > this.items.length) return false;

    this.position = x;
    return this.fill( this.items[x] );
}

/*
 *  Loop through all the items in the model
 */
Model.prototype.each = function(callback) {
    for (var i = 0; i < this.items.length; i++) {
        callback( this.item(i), i );
    }

    return this;
}

/*
 *  If any results were found trigger the callback, only once
 */
Model.prototype.any = function(callback) {
    if (this.items.length > 0) callback();

    return this;
}

/*
 *  If there were no results trigger the callback
 */
Model.prototype.empty = function(callback) {
    if (this.items.length == 0) callback();

    return this;
}

/*
 *  Remove all data from the model, ready to fill it again
 */
Model.prototype.reset = function() {
    this.clean = {};
    this.dirty = {};

    // TODO: Clean relations
}

/*
 *  Take a string or array of related models to include
 */
Model.prototype['with'] = function(_withs) {
    // Make sure we can treat it as an array
    var withs = (typeof _withs == 'object') ? _withs : [_withs];

    for (var i = 0; i < withs.length; i++) {
        this.withs.push( withs[i] );
    }

    return this;
}

/*
 *  Specify params for the call
 */
Model.prototype.where = function(key, value) {
    // Make dictionary from two strings
    if (typeof key == 'string') {
        var data = {};
        data[key] = value;
    // Or use the first item as an object
    } else {
        var data = key;
    }

    this.params = Api.mergeObjects( this.params, data );

    return this;
}

/*
 *  Restrict results to a single or array of statuses
 *  TODO: I don't think the items are being encoded correctly if this is an array
 *  It might be OK, or it might be the server which isn't handling it correctly
 */
Model.prototype.status = function(statuses) {
    return this.where('status', statuses);
}

/*
 *  Make the call!
 */
Model.prototype.get = function(callback) {
    this.call(callback);
}

/*
 *  Find an item by ID
 */
Model.prototype.find = function(callback, id) {
    this.where('id', id).get(callback);
}

/*
 *  Submit any changes back to the API for saving
 */
Model.prototype.save = function(callback, params) {
    var data = this.toObject();
    // Should we add passed data in?
    if (params) {
        for (var i in params) data[i] = params[i]
    }
    this.call(callback, '', data, 'POST');
}

/*
 *  Return BOOL indicating if the model exists or not
 */
Model.prototype.exists = function() {
    return (this.field('id') !== undefined);
}


/*
 *  Change the status of the object using the /model/status endpoint
 *  This triggers any cascading changes in related models
 */
Model.prototype.set_status = function(callback, status, _id) {
    if (!this.has_status) throw new Error(typeof(this)+' does not use the /model/status endpoint');

    if (this.exists() && !_id) {
        id = this.field('id');

    } else if (!this.exists() && _id) {
        id = _id;

    } else {
        throw new Error('Invalid conditions for set_status()');
    }

    return this.call( callback, '/status', {id: id, status: status}, 'POST');
}

/*
 *  Return the requested field, either from the dirty array if it's changed, or from the clean array
 *  If a second parameter is given, set the field to that value
 */
Model.prototype.field = function(field, set) {
    // Update the field value
    if (set) {
        // We can only change mutable fields
        if (this.mutable.indexOf(field) !== -1) {
            this.dirty[field] = set;
        } else {
            return false;
        }

    // Just return the field valye
    } else {
        // We might need to check for a getter function
        var getter_func_name = 'get' + field.charAt(0).toUpperCase() + field.slice(1);

        if (this.dirty[field] !== undefined) {
            return this.dirty[field];

        } else if (this.clean[field] !== undefined) {
            return this.clean[field];

        } else if (this[getter_func_name]) {
            return this[getter_func_name]();

        } else {
            return undefined;
        }
    }
}

/*
 *  Getter method to return a text version of the current status, or the status value
 */
Model.prototype.getStatus_title = function() {
    console.log(this.status_titles);
    return this.status_titles[ this.field('status') ]+'!!' ?
            this.status_titles[ this.field('status') ] :
            this.field('status');
}

/*
 *  Return related content by name, first check if it already exists
 */
Model.prototype.relation = function(callback, name, request) {
    for (var i = 0; i < this.relations.length; i++) {
        // Is this the relation we're looking for?
        if (this.relations[i].key == name) {

            // Do we have this data already?
            if (this.relations[i].data) {
                callback( this.relations[i].data );
                return;

            // If not, we'll need to request it, unless that was forbidden
            } else if (request !== false) {
                this.relations[i].request( callback, this );
                return;
            }

        }
    }

    // If we're here we didn't find the relation
    callback(false);
}

/*
 *  Return the current item as a dictionary, including changed content
 */
Model.prototype.toObject = function() {
    return Api.mergeObjects( this.clean, this.dirty );
}
