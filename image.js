
var Image = function() {
    return this;
}

Image.prototype = new Model();

Image.prototype.path = 'images';

Image.prototype.has_status = true;

Image.prototype.fields = ['id', 'company_id', 'user_id', 'questionnaire_id', 'question_id', 'answer_id', 'status', 'mime', 'width', 'height', 'filesize', 'url', 'orig_filename', 'filename', 'progress', 'title', 'created_at', 'updated_at', 'type', 'expires', 'url_original', 'slug'];

Image.prototype.mutable = ['title', 'status'];

Image.prototype.relations = [
    {
        key: 'company',
        model: 'Company',
        multiple: false,
        request: function(callback, item) {
            new Company().find(callback, item.field('company_id'));
        }
    },
    {
        key: 'user',
        model: 'User',
        multiple: false,
        request: function(callback, item) {
            new User().find(callback, item.field('user_id'));
        }
    },
    {
        key: 'questionnaire',
        model: 'Questionnaire',
        multiple: false,
        request: function(callback, item) {
            new Questionnaire().find(callback, item.field('questionnaire_id'));
        }
    },
    {
        key: 'question',
        model: 'Question',
        multiple: false,
        request: function(callback, item) {
            new Question().find(callback, item.field('question_id'));
        }
    },
    {
        key: 'answer',
        model: 'Answer',
        multiple: false,
        request: function(callback, item) {
            new Answer().find(callback, item.field('answer_id'));
        }
    }
];
