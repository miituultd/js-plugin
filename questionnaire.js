
var Questionnaire = function() {
    return this;
}

Questionnaire.prototype = new Model();

Questionnaire.prototype.path = 'questionnaires';

Questionnaire.prototype.has_status = true;

Questionnaire.prototype.fields = ['id', 'company_id', 'name', 'description', 'status', 'created_at', 'updated_at', 'user_id', 'disclaimer', 'thankyou', 'code', 'search'];

Questionnaire.prototype.mutable = ['name', 'description', 'status', 'disclaimer', 'thankyou'];

Questionnaire.prototype.relations = [
    {
        key: 'questions',
        model: 'Question',
        multiple: true,
        request: function(callback, item) {
            new Question().where('questionnaire_id', item.field('id')).get(callback);
        }
    }
];
