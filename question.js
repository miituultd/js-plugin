
var Question = function() {
    return this;
}

Question.prototype = new Model();

Question.prototype.path = 'questions';

Question.prototype.fields = ['id', 'questionnaire_id', 'media_id', 'name', 'accepts', 'text', 'multichoice', 'required', 'status', 'order', 'created_at', 'updated_at', 'type', 'maximum_selections', 'user_id', 'search'];

Question.prototype.has_status = true;

Question.prototype.relations = [
    {
        key: 'questionnaire',
        model: 'Questionnaire',
        multiple: false
    }
];
