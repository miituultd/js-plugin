
var Answer = function() {
    return this;
}

Answer.prototype = new Model();

Answer.prototype.path = 'answers';

Answer.prototype.has_status = true;

Answer.prototype.fields = ['id', 'question_id', 'respondent_id', 'media_id', 'text', 'type', 'multichoice', 'status', 'order', 'created_at', 'updated_at', 'rating', 'company_id', 'featured', 'description', 'search'];

Answer.prototype.mutable = ['text', 'multichoice', 'status', 'rating', 'featured', 'description'];

Answer.types = ['text', 'image', 'audio', 'video', 'multichoice', 'document'];

Answer.prototype.relations = [
    {
        key: 'media',
        model: 'Media',
        multiple: true,
        request: function(callback, item) {
            throw new Error('Cannot yet request media from answer.');
        }
    },
    {
        key: 'images',
        model: 'Image',
        multiple: true,
        request: function(callback, item) {
            return new Image().where('answer_id', item.field('id')).get(callback);
        }
    },
    {
        key: 'question',
        model: 'Question',
        multiple: false,
        request: function(callback, item) {
            throw new Error('Cannot yet request question from answer.');
        }
    },
    {
        key: 'respondent',
        model: 'Respondent',
        multiple: false,
        request: function(callback, item) {
            new Respondent().find( callback, item.field('id') );
        }
    },
    {
        key: 'transcripts',
        model: 'Transcript',
        multiple: true,
        request: function(callback, item) {
            throw new Error('Cannot yet request transcripts from answer.');
        }
    }
];
