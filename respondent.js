
var Respondent = function() {
    return this;
}

Respondent.prototype = new Model();

Respondent.prototype.path = 'respondents';

Respondent.prototype.has_status = true;

Respondent.prototype.fields = ['id', 'questionnaire_id', 'ip_address', 'created_at', 'updated_at', 'status', 'progress', 'accepted', 'search'];

Respondent.prototype.mutable = [];

Respondent.prototype.relations = [
    {
        key: 'answers',
        model: 'Answer',
        multiple: true,
        request: function(callback, item) {
            new Answer().where('respondent_id', item.field('id')).get(callback);
        }
    },
    {
        key: 'userfieldanswers',
        model: 'Userfieldanswer',
        multiple: true,
        request: function(callback, item) {
            new Userfieldanswer().where('respondent_id', item.field('id')).get(callback);
        }
    }
];

Respondent.prototype.getName = function() {

    var name = null;
    var first_answer = null;

    this.relation(function(userfieldanswers) {
        userfieldanswers.each(function(userfieldanswer, i) {

            if (i == 0) first_answer = userfieldanswer.field('answer');

            userfieldanswer.relation(function(userfield) {

                if (userfield.field('type') == Userfield.TYPE_NAME) {
                    name = userfieldanswer.field('answer');
                }

            }, 'userfield', false);

        });
    }, 'userfieldanswers', false);

    return name ? name : first_answer;
}

/*
 *  Indicate whether agreement has been specified for the disclaimer
 */
Respondent.disclaimer = function(callback, accepted, id) {
    return new Respondent().call(callback, '/disclaimer', {
        'id': id,
        'accepted': accepted
    }, 'POST');
}

/*
 *  TODO: Port over from PHP
 *  Create a new respondent with the provided questionnaire ID and userfields
 *
public function _create( $questionnaire_id, $userfields) {
    return $this->call('', array(
        'questionnaire_id' => $questionnaire_id,
        'userfields'       => $userfields
    ), 'POST');
}
*/

