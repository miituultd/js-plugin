
var User = function() {
    return this;
}

User.prototype = new Model();

User.prototype.path = 'users';

User.prototype.fields = ['id', 'company_id', 'forename', 'surname', 'email', 'email_verified', 'email_verify_token', 'password', 'status', 'created_at', 'updated_at', 'level_id', 'permissions'];

User.prototype.mutable = ['forename', 'surname', 'email', 'password'];

User.prototype.getFullname = function() {
    return this.field('forename')+' '+this.field('surname');
}
